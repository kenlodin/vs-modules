#include <core/event/event.hh>
#include <core/module/module.hh>
#include <core/event/eventManager.hh>
#include <boost/bind.hpp>

MODULE_AUTHORS("Corentin DERBOIS");
MODULE_DESC("Count all new connexion");

class ConnexionCounter : public BaseEvent
{
  public:
    ConnexionCounter()
    {
      count = 0;
      EventManager::getInstance()->attach("auth.master",
                                          boost::bind(&ConnexionCounter::counter,
                                          this, _1));
    }

    int counter(std::vector<boost::any> params)
    {
      count++;
      std::cout << "ConnexionCounter [" << count << "]" << std::endl;
      return 0;
    }
 
  private:
    int count;
};

MAIN_CLASS(ConnexionCounter)
