#ifndef EVENT_AUTH_HH_
# define EVENT_AUTH_HH_

# include <SFML/Network.hpp>
# include <core/event/event.hh>
# include <core/event/eventManager.hh>
# include <core/module/module.hh>
# include <core/sqlManager/SqlManager.hh>
# include <core/network/ClientList.hh>
# include <core/tracker/Tracker.hh>

class EventAuth : public BaseEvent
{
  public:
    EventAuth();
    int connect(std::vector<boost::any> params);
};

#endif /* !EVENT_AUTH_HH_ */
